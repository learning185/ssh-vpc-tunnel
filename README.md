# ssh-private-vpc

source : https://towardsdatascience.com/connecting-to-an-ec2-instance-in-a-private-subnet-on-aws-38a3b86f58fb


## connect

### ssh tunnel

 ssh -i "terraform.pem" -J ubuntu@13.36.165.34 ubuntu@10.0.0.31

### database connection

you can connect with the database in a private network throught the jumpserver
This can be done automatically with the program "DBeaver"

> Note : this only work with rsa key set. the pem key of aws don't work

#### setup 

setup a connection

    - create a new databse connection in "DBeaver"
    - select the database family
    - in tab main
        - host : rds endpoint
        - port : rds port
        - username : username of the database server
        - password : password of the database server
    - in tab ssh
        - Host/ip : the ip adress of the jump server
        - Username : the username you have access to on the jump server
        - authentication Method: Public Key
        - Private Key : path to your private rsa key ( that has acces to the jump server )
        - Passphrase : the passphrase of your keyset ( if not configured leave black )


