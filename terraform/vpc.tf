resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "public-subnet" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-west-3a"

  tags = merge(
    var.project_tags,
    {
      Name = "public-subnet"
    }
  )
}

resource "aws_subnet" "private-subnet-eu-west-3a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.10.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-west-3a"

  tags = merge(
    var.project_tags,
    {
      Name = "private-subnet-eu-west-3a"
    }
  )
}


resource "aws_subnet" "private-subnet-eu-west-3b" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.20.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "eu-west-3b"

  tags = merge(
    var.project_tags,
    {
      Name = "private-subnet-eu-west-3b"
    }
  )
}

resource "aws_internet_gateway" "internet-gw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.project_tags,
    {
      Name = "internet-gw"
    }
  )
}

resource "aws_db_subnet_group" "rds_subnets" {
  name       = "rds_subnet_group"
  subnet_ids = [
    aws_subnet.private-subnet-eu-west-3a.id,
    aws_subnet.private-subnet-eu-west-3b.id,
    ]

  tags = merge(
    var.project_tags,
    {
      Name = "rds_subnet_group"
    }
  )
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gw.id
  }
}

resource "aws_route_table_association" "public-rta" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}


