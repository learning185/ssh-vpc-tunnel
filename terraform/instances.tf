resource "aws_instance" "bastion-instance" {
  ami           = var.instance-ami
  instance_type = var.instance-type

  subnet_id = aws_subnet.public-subnet.id

  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  key_name = aws_key_pair.mykeypair.key_name

  tags = merge(
    var.project_tags,
    {
      Name = "bastion-instance"
    }
  )
}

resource "aws_instance" "private-instance" {
  ami           = var.instance-ami
  instance_type = var.instance-type

  subnet_id = aws_subnet.private-subnet-eu-west-3a.id

  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  key_name = aws_key_pair.mykeypair.key_name

  tags = merge(
    var.project_tags,
    {
      Name = "private-instance"
    }
  )
}

resource "aws_key_pair" "mykeypair" {
  key_name   = "terraform"
  public_key = file(var.key_path)
}

