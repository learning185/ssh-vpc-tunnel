variable "region" {
  default = "eu-west-3"
}

variable "profile" {
  default = "terraform"
}

variable "instance-ami" {
  default = "ami-0f7cd40eac2214b37"
}

variable "key_path" {
  default = "~/.ssh/id_rsa.pub"
}

variable "project_tags" {
  default = {
    usecase  = "learning"
    tutorial = "ssh-private-vpc"
  }
  description = "Additional project tags"
  type        = map(string)
}

variable "instance-type" {
  default = "t2.nano"
}

variable "db_password" {
  description = "RDS root user password"
  type        = string
  sensitive   = true
}
